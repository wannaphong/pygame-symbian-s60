# READ-ONLY: This project has been archived. from [https://code.google.com/p/pygame-symbian-s60/](https://code.google.com/p/pygame-symbian-s60/) #

This project serves as a file repository and issue tracker for pygame for S60 releases and dependencies.

After installing, try out this simple snake game. The PyS60 snake game converted to pygame. Copy the file to \data\pygame\apps\ on your phone and it will show up in launcher's Applications list.

	
Snake on Vista	Snake on Nokia E51
There is also a PyBox2D physics library package for PyS60 available for download and an example using pygame( works on PC as well if you have pygame and PyBox2D installed ).

Something about the launcher
When application is selected the launcher starts a PyS60 background server and closes itself. The server waits for the launcher to close and starts a new pygame process for the selected application. This ensures that the application has a clean pygame environment. If you encounter any problems, the launcher is configured to write standard output to \data\pygame\stdout.txt and applications started with the launcher write to appout.txt.

Roadmap
Use PyS60 application packaging tool
Split the pygame runtime and launcher into separate packages
Create PyS60 library package
Use the package to create the launcher application
Launcher should have all the extensions of PyS60 and pygame
Improve startup speed
Byte-compiled and zipped pygame library
Make tests run on phone and simulator
Make mixer.music work ( audio streaming )
Crashes in SDL currently
Tools for creating stand-alone pygame applications
Enable pygame joystick to handle acceleration sensor
Enable pygame camera to use phone's camera(s)
See if at least notes and queries from appuifw work with pygame
Ensure OpenGL ES works with pygame
Sources
The pygame sources are available at the official pygame repository. See: http://www.pygame.org/

This project stores the sources of used external libraries. See the how_to_build.txt in pygame SVN about how to build the project.

PyBox2D build scripts for Symbian can also be found from SVN.